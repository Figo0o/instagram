//
//  TestImagesDtoConverter.swift
//  InstagramTests
//
//  Created by Ahmed Farrag on 06.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import XCTest
@testable import Instagram

class TestImagesDtoConverter: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testWhenParsingInstagramImagesWithCorrectDictionaryThenReturnArrayOfMedia() {
        // given
        let dtoMedia: [InstagramMediaDTO]? = JsonReader.readFrom(fileName: "InstagramImages")
        
        // when 
        let media = InstagramMediaConverter.convert(dtos: dtoMedia!)
        
        // then
        let firstImage = media[0]
        XCTAssert(media.count == 20)
        XCTAssert(firstImage.width == 640)
        XCTAssert(firstImage.height == 800)
        XCTAssert(firstImage.imagePath == "https://scontent.cdninstagram.com/vp/57db15397ec045c7d2d52d63c4c966da/5E25064C/t51.2885-15/sh0.08/e35/p640x640/69621914_433927787530927_686314611519187226_n.jpg?_nc_ht=scontent.cdninstagram.com")

    }
    
    func testWhenParsingEmptyDTOArrayThenReturnEmptyModelArray() {
        // given & when
        let media = InstagramMediaConverter.convert(dtos: [InstagramMediaDTO]())
        
        // then
        XCTAssert(media.count == 0)
    }

}
