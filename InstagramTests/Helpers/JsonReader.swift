//
//  JsonReader.swift
//  InstagramTests
//
//  Created by Ahmed Farrag on 06.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation

class JsonReader {
    class func readFrom<T: Decodable>(fileName: String) -> T? {
        let testBundle = Bundle(for: type(of: JsonReader().self) as AnyClass)

        if let path = testBundle.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                return try? decoder.decode(T.self, from: data)
            } catch {
                return nil
            }
        }

        return nil
    }
}
