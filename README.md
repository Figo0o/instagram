# Instagram
### Light
![light](https://gitlab.com/Figo0o/instagram/raw/master/src/light_screenshot.png)
### Dark
![dark](https://gitlab.com/Figo0o/instagram/raw/master/src/dark_screenshot.png)

# Description
Dark mode Instagram app, a photo gallery app implemented only to display your posts. 

# Features: 
* Instagram authentication: Integration with Instagram public API to fetch the user’s posts images/videos. [Instagram Developer Documentation](https://www.instagram.com/developer/) 
* `FeedsViewController`: Displays the user’s posted images vertically.
* `ImageDetailViewController`: Display the selected image in a separate view controller.
* Custom transition: A nice looking transition when you open or dismiss one of the user’s images.
* Unit Tests: Covered the parsing part. ✅
* UI Tests: Always increasing coverage is so important. ✅
* Dark mode: Why not to support dark mode. ❤️

# Nice to have: 
- Pull down to refresh.
- Show error state (alert) when getting an error response.
- Show loading indicator while making a request.
- Handle empty screen (When there are no posts for the user).
- Offline support: This is to cache all the user posts content data using core data and when initial open the application load all the home data from disk then fetch it from Instagram if there is an internet connection and only load the difference. 
- Pagination: To load the posts content data with pagination. 
- Support video play: As on Instagram you can post an image and videos, it’s nice to have to also support watching videos.
- Interactive `ImageDetailViewController` transition: to dismiss the view controller interactively. 
- Localization: as if we are supporting multiple countries.

# Issues: (new)

- [SOLVED] there is a problem while emebeding a cocoapods framework inside a dynamic framework running on a real device. Will switch to carthage to fix that out. or gonna generate a podspec for the submodules to fix this issue.

# How to make it work: 
Just download and run the project. 😉

As this is a sandbox application I already created credentialed to login with. 
- Username: amohamed9
- Password: Ahmed@1991

**Or**

Just send me your Instagram account and I will invite you to the application. :)

# Application architecture and structure: 
* I'm following the **MVVM-C** design pattern, and as I want to implement the persistence layer I would like to include a repository layer to it.
Why I chose MMVM-C? Because it presents a good separation of concern, easy to test and helps you to scale.
Also, I'm not using pure MVVM but also I'm using Coordinators that have the presentation logic inside.
For me it's a little bit simpler than VIPER as in viper I have to create 5 components in each module. That's why in this small application I decided to go for MVVM-C.
* Modularization: Split the project into `Instagram`, `InstagramCore` and `InstagramOauth`. There are a lot of reasons and benefits why to do so. 
    * it speeds up the project build time especially when you have a huge project with a huge codebase, this will help to reduce the build time especially if the code base is in swift. :) 
    * Good structure to decouple and separate your code, so in this way you are forced to write a clean code because if you tried to couple it the compiler will complain.
    * Forcing devs to use the same *core* components (ex: UI and networking) or even modify them without creating new ones.
* I’m also do not prefer using storyboards and you can tell from the native UI and constraints implementation. Why? Because it's more flixable to work with larger team without conflicts (storyboard conflicts are very bad) and also it does not take long time to compile.

## Let's discuss a little bit the App current modules: 

* `InstagramCore`: Contains everrything shared among all modules (Extensions, Teusable components and Network layer).
* `InstagramOauth`: Contains Instagram oauthentication logic.
* `Instagram`: This is the app target itself.

# Third-party used: 
I’m not a big fan of using third-parties especially if I don’t know what they are doing under the hood, but sometimes it’s ok to use some if it’s going to speed up the implementation process. 

I’m currently using only two libraries and the plan is to remove them with native implementation.  

- [Alamofire](https://github.com/Alamofire/Alamofire) is an HTTP networking library written in swift. 
- [Kingfisher](https://github.com/onevcat/Kingfisher) pure swift library for downloading and caching images.

# Finally: 
All feedback or contribution is welcome. ❤️
