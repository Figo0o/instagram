//
//  Observable.swift
//  InstagramCore
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation

public class Observable<T> {
    
    public init() { }
    
    public typealias ObservableCallBack = (T?) -> Void

    var observers: [ObservableCallBack] = [ ]
    
    public var value: T? {
        didSet {
            observers.forEach { (observer) in
                observer(self.value)
            }
        }
    }
    
    public func observe(observer: @escaping ObservableCallBack) {
        observers.append(observer)
    }
}
