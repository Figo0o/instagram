//
//  UIViewController+Extension.swift
//  InstagramCore
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit

public extension UIViewController {
    func addCloseButton() {
        let closeButton = UIBarButtonItem(image: UIImage.Core.image(named: "close"),
                                          style: .done,
                                          target: self,
                                          action: #selector(closeAction(_:)))
        navigationItem.leftBarButtonItem = closeButton
    }
    
    @objc func closeAction(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
}
