//
//  Builder.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

// more info: https://gist.github.com/nicklockwood/9b4aac87e7f88c80e932ba3c843252df

import UIKit

public protocol Builder {}

public extension Builder where Self: UIView {
    init(with config: (inout Self) -> Void) {
        self.init()
        self.translatesAutoresizingMaskIntoConstraints = false
        config(&self)
    }
}

extension UIView: Builder {}
