//
//  WKWebView+Extension.swift
//  InstagramCore
//
//  Created by Ahmed Farrag on 06.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import WebKit

public extension WKWebView {
    class func clean() {
        HTTPCookieStorage.shared.removeCookies(since: Date.distantPast)
        
        WKWebsiteDataStore.default().fetchDataRecords(ofTypes: WKWebsiteDataStore.allWebsiteDataTypes()) { records in
            records.forEach { record in
                WKWebsiteDataStore.default().removeData(ofTypes: record.dataTypes, for: [record], completionHandler: {})
            }
        }
    }
}
