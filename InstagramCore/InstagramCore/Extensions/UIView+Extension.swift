//
//  UIView+Extension.swift
//  Shared
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit

public extension UIView {
    func addSubviews(_ views: UIView...) {
        for view in views {
            addSubview(view)
        }
    }
    
    func pinToSuperView() {
        guard let superview = superview else { return }
        
        leadingAnchor.constraint(equalTo: superview.leadingAnchor).isActive = true
        trailingAnchor.constraint(equalTo: superview.trailingAnchor).isActive = true
        topAnchor.constraint(equalTo: superview.topAnchor).isActive = true
        bottomAnchor.constraint(equalTo: superview.bottomAnchor).isActive = true
    }
    
    func leadingTrailingConstraints(constant: CGFloat) {
        guard let superview = superview else { return }

        leadingAnchor.constraint(equalTo: superview.leadingAnchor, constant: constant).isActive = true
        trailingAnchor.constraint(equalTo: superview.trailingAnchor, constant: -constant).isActive = true
    }
}
