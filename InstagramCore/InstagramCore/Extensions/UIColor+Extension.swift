//
//  UIColor+Extension.swift
//  InstagramCore
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit

public extension UIColor {
    enum Instagram {
        public static var `default`: UIColor {
            if #available(iOS 13.0, *) {
                return .label
            } else {
                return .black
            }
        }
        
        public static var systemBackground: UIColor {
            if #available(iOS 13.0, *) {
                return .systemBackground
            } else {
                return .white
            }
        }
        
        public static var systemGray3: UIColor {
            if #available(iOS 13.0, *) {
                return .systemGray3
            } else {
                return .gray
            }
        }
    }
}

