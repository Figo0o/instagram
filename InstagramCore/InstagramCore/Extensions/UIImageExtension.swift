//
//  UIImageExtension.swift
//  InstagramCore
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit

public extension UIImage {
    enum Core {
        public static func image(named name: String) -> UIImage? {
            return UIImage(named: name, in: Bundle(identifier: "com.farrag.InstagramCore"), compatibleWith: nil)
        }
    }
}

