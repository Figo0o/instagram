//
//  Networking.swift
//  Shared
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation
import Alamofire

public let BaseUrl = "http://afarrag.me/api/v1/"

public enum NetworkErrors: Error {
    case unknown(message: String)
}

public class Networking {
    
    public static func dataRequest(baseURL: String = BaseUrl,
                                   path: String,
                                   method: HTTPMethod,
                                   headers: [String: String]?,
                                   params: [String: Any]?,
                                   completion: @escaping (Swift.Result<Data, NetworkErrors>) -> (Void)) {
        Alamofire.request("\(baseURL)\(path)",
                          method: method,
                          parameters: params,
                          encoding: URLEncoding.default,
                          headers: headers)
            .validate(statusCode: 200..<300)
            .responseData { (response) in
                switch response.result {
                case .success(let data):
                    completion(.success(data))
                case .failure(_):
                    switch response.response?.statusCode {
                    case 404,
                         401,
                         403,
                         500,
                         100,
                         .none,
                         .some(_):
                        completion(.failure(NetworkErrors.unknown(message: "Something went wrong!")))
                    }
                }
        }
    }
}
