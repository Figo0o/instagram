//
//  ResponseDTO.swift
//  UserApp
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation

public struct ResponseDTO<T: Decodable>: Decodable {
    public var code: Int?
    public var data: T?
}
