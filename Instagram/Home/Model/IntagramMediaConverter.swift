//
//  IntagramMediaConverter.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation

class InstagramMediaConverter {
    static func convert(dtos: [InstagramMediaDTO]) -> [Media] {
        dtos.compactMap { (dto) -> Media? in
            guard let imagePath = dto.images?.standardResolution?.url,
                let width = dto.images?.standardResolution?.width,
                let height = dto.images?.standardResolution?.height else { return nil }
            
            return Media(imagePath: imagePath, width: width, height: height)
        }
    }
}
