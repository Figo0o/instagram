//
//  File.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation

class Media {
    var imagePath: String
    var width: Int
    var height: Int
    
    init(imagePath: String, width: Int, height: Int) {
        self.imagePath = imagePath
        self.width = width
        self.height = height
    }
}
