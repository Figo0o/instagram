//
//  InstagramMediaDTO.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation

struct InstagramMediaDTO: Decodable {
    struct ImageResolutionDTO: Decodable {
        
        struct ImageDTO: Decodable {
            var height: Int?
            var width: Int?
            var url: String?
        }
        
        var lowResolution: ImageDTO?
        var standardResolution: ImageDTO?
        var thumbnail: ImageDTO?
    }
    
    var images: ImageResolutionDTO?
}
