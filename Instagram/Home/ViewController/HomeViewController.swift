//
//  HomeViewController.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    private var viewModel: HomeViewModel
    
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.delegate = self
        tableView.dataSource = self.viewModel
        tableView.separatorColor = .clear
        tableView.register(PostTableViewCell.self, forCellReuseIdentifier: PostTableViewCell.reuseIdentifier)
        return tableView
    }()
    
    init(viewModel: HomeViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Instagram"
        setupBindings()
        viewModel.fetchInstagramMedia()
        
        view.addSubview(tableView)
        tableView.pinToSuperView()
    }
    
    private func setupBindings() {
        viewModel.didChangeDataSource = { [weak self] in
            self?.tableView.reloadData()
        }
    }
}

extension HomeViewController: ZoomAnimatorDelegate {
    func transitionWillStartWith(zoomAnimator: CustomAnimator) {
        guard let selectedIndex = tableView.indexPathForSelectedRow,
            let selectedCell = tableView.cellForRow(at: selectedIndex) as? PostTableViewCell else { return }
        
        let cellFrame = tableView.convert(selectedCell.frame, to: self.view)

        if cellFrame.minY < self.view.frame.minY {
            tableView.scrollToRow(at: selectedIndex, at: .top, animated: true)
        } else if cellFrame.maxY > self.view.frame.maxY {
            tableView.scrollToRow(at: selectedIndex, at: .bottom, animated: true)
        }
    }
    
    func referenceImageView(for zoomAnimator: CustomAnimator) -> UIImageView? {
        guard let selectedIndex = tableView.indexPathForSelectedRow,
            let selectedCell = tableView.cellForRow(at: selectedIndex) as? PostTableViewCell else { return nil }

        return selectedCell.imageView
    }
    
    func referenceImageViewFrameInTransitioningView(for zoomAnimator: CustomAnimator) -> CGRect? {
        guard let selectedIndex = tableView.indexPathForSelectedRow,
            let selectedCell = tableView.cellForRow(at: selectedIndex) as? PostTableViewCell else { return nil }

        let cellFrame = tableView.convert(selectedCell.frame, to: self.view)
        if cellFrame.minY < self.tableView.contentInset.top {
            return CGRect(x: cellFrame.minX, y: self.tableView.contentInset.top, width: cellFrame.width, height: cellFrame.height)
        }
        return cellFrame
    }
}

extension HomeViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectItem(at: indexPath.row)
    }
}
