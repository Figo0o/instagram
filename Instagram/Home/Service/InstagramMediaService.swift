//
//  InstagramMediaService.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import InstagramCore

class InstagramMediaService {
    init() {}
    
    func media(for accessToken: String, completion: @escaping (Result<[Media], NetworkErrors>) -> (Void)) {
        Networking.dataRequest(baseURL: "https://api.instagram.com/v1/",
                               path: "users/self/media/recent",
                               method: .get,
                               headers: nil,
                               params: ["access_token": accessToken]) { (resulst) -> (Void) in
                                switch resulst {
                                case .success(let data):
                                    let decoder = JSONDecoder()
                                    decoder.keyDecodingStrategy = .convertFromSnakeCase
                                    if let responseDTO = try? decoder.decode(ResponseDTO<[InstagramMediaDTO]>.self, from: data),
                                        let mediaDTO = responseDTO.data {
                                        completion(.success(InstagramMediaConverter.convert(dtos: mediaDTO)))
                                    } else {
                                        completion(.failure(NetworkErrors.unknown(message: "Something went wrong!")))
                                    }

                                case .failure(let error):
                                    completion(.failure(error))
                                }
        }
    }
}
