//
//  HomeCoordinator.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit

class HomeCoordinator: NSObject, UIViewControllerTransitioningDelegate {
    weak var presentingViewController: HomeViewController?
    var imageDetailViewController: ImageDetailViewController?

    let transition = CustomAnimator()

    func openImage(with path: String) {
        let coordinator = ImageDetailCoordinator()
        let viewModel = ImageDetailViewModel(coordinator: coordinator, imagePath: path)
        let viewController = ImageDetailViewController(viewModel: viewModel)
        coordinator.presentingViewController = viewController
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.modalPresentationStyle = .fullScreen
        
        navigationController.transitioningDelegate = self
        
        imageDetailViewController = viewController
        presentingViewController?.present(navigationController,
                                          animated: true,
                                          completion: nil)
    }
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard
            let homeViewController = presentingViewController,
            let detailViewController = imageDetailViewController
            else { return nil }
                
        transition.isPresenting = true
        transition.toDelegate = detailViewController
        transition.fromDelegate = homeViewController
        
        return transition
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        guard
            let homeViewController = presentingViewController,
            let detailViewController = imageDetailViewController
            else { return nil }
                
        transition.isPresenting = false
        transition.toDelegate = homeViewController
        transition.fromDelegate = detailViewController
        
        return transition
    }
}
