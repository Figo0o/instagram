//
//  PostView.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit
import Kingfisher

class PostView: UIView {

    var imagePath: String? {
        set {
            guard let path = newValue else { return }
            imageView.kf.setImage(with: URL(string: path))
        }
        get { return "" }
    }
    
    var height: CGFloat = 200.0 {
        didSet {
            heightConstraint?.constant = self.height
        }
    }
    
    let imageView = UIImageView {
        $0.contentMode = .scaleAspectFit
    }
    
    private var heightConstraint: NSLayoutConstraint?
    
    init() {
        super.init(frame: .zero)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(imageView)
    }
    
    private func setupConstraints() {
        imageView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
        imageView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
        heightConstraint = imageView.heightAnchor.constraint(equalToConstant: 200.0)
        heightConstraint?.isActive = true
        imageView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        let bottomConstraint = imageView.bottomAnchor.constraint(equalTo: bottomAnchor)
        bottomConstraint.priority = .defaultLow
        bottomConstraint.isActive = true
    }
}
