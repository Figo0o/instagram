//
//  PostTableViewCell.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//


import UIKit

class PostTableViewCell: UITableViewCell {
    
    var imagePath: String? {
        set { postView.imagePath = newValue }
        get { return postView.imagePath }
    }
    
    var height: CGFloat = 200.0 {
        didSet {
            postView.height = self.height
        }
    }
    
    private let postView = PostView { _ in
        
    }
    
    override var imageView: UIImageView? {
        get { return postView.imageView }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(postView)
    }
    
    private func setupConstraints() {
        postView.pinToSuperView()
    }

}

extension PostTableViewCell {
    func configure(media: Media) {
        let newHeight = (UIScreen.main.bounds.width * CGFloat(media.height)) / CGFloat(media.width)
        imagePath = media.imagePath
        height = newHeight
    }
}
