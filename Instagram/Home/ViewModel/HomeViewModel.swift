//
//  HomeViewModel.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit

class HomeViewModel: NSObject {
    
    var didChangeDataSource: (() -> Void)?
    
    // MARK:- private
    
    private let coordinator: HomeCoordinator
    private let accessToken: String
    private let mediaService: InstagramMediaService
    private var media = [Media]() {
        didSet {
            self.didChangeDataSource?()
        }
    }
    
    init(coordinator: HomeCoordinator,
         accessToken: String,
         mediaService: InstagramMediaService) {
        self.coordinator = coordinator
        self.accessToken = accessToken
        self.mediaService = mediaService
    }
    
    func fetchInstagramMedia() {
        mediaService.media(for: accessToken) { [weak self] (result) -> (Void) in
            switch result {
            case .success(let media):
                self?.media = media
            case .failure(let error):
                // TODO: handle error state (showing alert for example)
                // remove print
                print(error)
            }
        }
    }
    
    func didSelectItem(at index: Int) {
        coordinator.openImage(with: media[index].imagePath)
    }
}

extension HomeViewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: PostTableViewCell = tableView.dequeueReusableCell(for: indexPath)
        cell.configure(media: media[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return media.count
    }
}
