//
//  ImageDetailViewController.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit
import Kingfisher
import InstagramCore

class ImageDetailViewController: UIViewController {
    private let viewModel: ImageDetailViewModel
    
    private var imageView = UIImageView {
        $0.translatesAutoresizingMaskIntoConstraints = false
        $0.contentMode = .scaleAspectFit
        $0.isUserInteractionEnabled = true
    }
    
    init(viewModel: ImageDetailViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.Instagram.systemGray3
        title = "image"
        addCloseButton()
        setupBindings()
        viewModel.postObservableSetup()
        
        view.addSubview(imageView)
        imageView.pinToSuperView()
    }
    
    private func setupBindings() {
        viewModel.imagePath.observe { [weak self] (string) in
            guard let string = string else { return }
            self?.imageView.kf.setImage(with: URL(string: string))
        }
    }
}

extension ImageDetailViewController: ZoomAnimatorDelegate {
    func transitionWillStartWith(zoomAnimator: CustomAnimator) {
        // TODO: to be implemented if needed
    }
    
    func referenceImageView(for zoomAnimator: CustomAnimator) -> UIImageView? {
        return imageView
    }
    
    func referenceImageViewFrameInTransitioningView(for zoomAnimator: CustomAnimator) -> CGRect? {
        return CGRect(x: 0,
                      y: 0,
                      width: UIScreen.main.bounds.size.width,
                      height: UIScreen.main.bounds.size.height)
    }
}
