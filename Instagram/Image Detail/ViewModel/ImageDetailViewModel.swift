//
//  ImageDetailViewModel.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation
import InstagramCore

class ImageDetailViewModel {
    let imagePath = Observable<String>()
    
    private let coordinator: ImageDetailCoordinator
    
    init(coordinator: ImageDetailCoordinator, imagePath: String) {
        self.coordinator = coordinator
        self.imagePath.value = imagePath
    }
    
    func postObservableSetup() {
        imagePath.value = imagePath.value
    }
}
