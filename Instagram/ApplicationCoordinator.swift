//
//  ApplicationCoordinator.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit

class ApplicationCoordinator {
    weak var window: UIWindow?
    
    func goToApplication() {
        let coordinator = LoginCoordinator()
        let viewModel = LoginViewModel(coordinator: coordinator)
        let viewController = LoginViewController(viewModel: viewModel)
        coordinator.presentingViewController = viewController
        
        let navigationController = UINavigationController(rootViewController: viewController)
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
    }
}
