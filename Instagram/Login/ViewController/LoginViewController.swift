//
//  LoginViewController.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    private let viewModel: LoginViewModel
    private lazy var loginView: LoginView = {
        let view = LoginView()
        return view
    }()
        
    init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        view.backgroundColor = UIColor.Instagram.systemBackground
        
        setupBindings()
    }
    
    override func loadView() {
        view = loginView
    }
    
    private func setupBindings() {
        loginView.didTapLogin = { [weak self] in
            self?.viewModel.didTapLoginButton()
        }
    }
}
