//
//  LoginViewModel.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation
import InstagramOauth

class LoginViewModel {
    private let coordinator: LoginCoordinator
    
    init(coordinator: LoginCoordinator) {
        self.coordinator = coordinator
    }
    
    func didTapLoginButton() {
        guard let viewController = coordinator.presentingViewController else { return }
        
        InstagramOauth.shared.login(from: viewController) { [weak self] accessToken in
            self?.coordinator.goToHome(with: accessToken)
        }
    }
}
