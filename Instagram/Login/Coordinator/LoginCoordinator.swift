//
//  LoginCoordinator.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit

class LoginCoordinator {
    weak var presentingViewController: UIViewController?
    
    func goToHome(with acessToken: String) {
        let coordinator = HomeCoordinator()
        let viewModel = HomeViewModel(coordinator: coordinator,
                                      accessToken: acessToken,
                                      mediaService: InstagramMediaService())
        let viewController = HomeViewController(viewModel: viewModel)
        coordinator.presentingViewController = viewController
        let navigationController = UINavigationController(rootViewController: viewController)

        presentingViewController?.dismiss(animated: true,
                                          completion: {
                                            UIApplication.shared.keyWindow?.rootViewController = navigationController
        })
    }
}
