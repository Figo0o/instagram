//
//  LoginView.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit
import InstagramCore

class LoginView: UIView {
    
    // MARK: - public
    
    var didTapLogin: (() -> Void)?
    
    // MARK: - private
    
    private enum Constants {
        static let imageWitdth: CGFloat = 150.0
        static let imageHeight: CGFloat = 150.0
    }
    
    private let welcomeLabel = UILabel {
        $0.text = "Welcome to Instagram"
        $0.textAlignment = .center
        $0.textColor = UIColor.Instagram.default
        $0.font = UIFont.boldSystemFont(ofSize: 28.0)
        $0.setContentHuggingPriority(.defaultHigh, for: .vertical)
    }
    
    private let descriptionLabel = UILabel {
        $0.text = "Dark mode Instagram app to watch only your posts."
        $0.textAlignment = .center
        $0.numberOfLines = 0
        $0.textColor = .lightGray
        $0.setContentHuggingPriority(.defaultHigh, for: .vertical)
    }
    
    private let logoImageView = UIImageView {
        $0.image = UIImage(named: "icon")
    }
    
    private let loginButton = UIButton {
        $0.backgroundColor = UIColor.Instagram.systemGray3
        $0.setTitleColor(UIColor.Instagram.default, for: .normal)
        $0.setTitle("Login", for: .normal)
        $0.layer.cornerRadius = 8.0
        $0.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20.0)
        $0.addTarget(self, action: #selector(loginAction(_:)), for: .touchUpInside)
    }
    
    // MARK: - setup
    
    init() {
        super.init(frame: .zero)
        
        setupViews()
        setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews() {
        addSubview(welcomeLabel)
        addSubview(descriptionLabel)
        addSubview(logoImageView)
        addSubview(loginButton)
    }
    
    private func setupConstraints() {
        welcomeLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16.0).isActive = true
        welcomeLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16.0).isActive = true
        welcomeLabel.topAnchor.constraint(lessThanOrEqualTo: topAnchor, constant: 150.0).isActive = true
        
        descriptionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16.0).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16.0).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: welcomeLabel.bottomAnchor, constant: 16.0).isActive = true

        logoImageView.widthAnchor.constraint(equalToConstant: Constants.imageWitdth).isActive = true
        logoImageView.heightAnchor.constraint(equalToConstant: Constants.imageHeight).isActive = true
        logoImageView.centerXAnchor.constraint(equalTo: centerXAnchor).isActive = true
        logoImageView.topAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 50.0).isActive = true
        
        loginButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16.0).isActive = true
        loginButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -16.0).isActive = true
        loginButton.topAnchor.constraint(lessThanOrEqualTo: logoImageView.bottomAnchor, constant: 150.0).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 55.0).isActive = true
        loginButton.bottomAnchor.constraint(lessThanOrEqualTo: bottomAnchor, constant: -50.0).isActive = true

    }
    
    // MARK: - actions
    
    @objc func loginAction(_ sender: UIButton) {
        self.didTapLogin?()
    }
}
