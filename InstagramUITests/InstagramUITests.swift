//
//  InstagramUITests.swift
//  InstagramUITests
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import XCTest

class InstagramUITests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_1_FirstLogin() {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launchEnvironment = ["UITESTS":"1"]
        app.launch()
        
        XCTAssert(app.staticTexts["Welcome to Instagram"].exists == true)
        XCTAssert(app.staticTexts["Dark mode Instagram app to watch only your posts."].exists == true)

        app.buttons["Login"].tap()
        
        let userNameField = app.textFields["Phone number, username, or email"]
        waitForElementToAppear(userNameField)
        userNameField.tap()
        userNameField.typeText("amohamed9")
        
        app.buttons["Done"].tap()
        
        let passwordField = app.secureTextFields["Password"]
        waitForElementToAppear(passwordField)
        passwordField.tap()
        passwordField.typeText("Ahmed@1991")
        
        let loginButton = app.buttons["Log In"]
        waitForElementToAppear(loginButton)
        loginButton.tap()
        
        let saveInfoButton = app.buttons["Save Info"]
        waitForElementToAppear(saveInfoButton)
        saveInfoButton.tap()
        
        let navigationBarTitle = app.staticTexts["Instagram"]
        waitForElementToAppear(navigationBarTitle)
        
        app.swipeUp()
    }
    
    func test_2_NotFirstTimeLogin() {
        // UI tests must launch the application that they test.
        let app = XCUIApplication()
        app.launch()
        
        XCTAssert(app.staticTexts["Welcome to Instagram"].exists == true)
        XCTAssert(app.staticTexts["Dark mode Instagram app to watch only your posts."].exists == true)

        app.buttons["Login"].tap()
        
        let navigationBarTitle = app.staticTexts["Instagram"]
        waitForElementToAppear(navigationBarTitle)
        
        app.swipeUp()
    }
    
    // MARK: - Helpers
    
    func waitForElementToAppear(_ element: XCUIElement) {
        let predicate = NSPredicate(format: "exists == 1")
        expectation(for: predicate, evaluatedWith: element, handler: nil)

        waitForExpectations(timeout: 10, handler: nil)
    }

}
