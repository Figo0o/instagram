//
//  InstagramIntegrationViewController.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import UIKit
import WebKit

struct API {
    static let INSTAGRAM_AUTHURL = "https://api.instagram.com/oauth/authorize/"
    static let INSTAGRAM_CLIENT_ID = "9ccdebbb35634d06bfc4f9c0ffcdf2e8"
    static let INSTAGRAM_CLIENTSERCRET = "082f2fe8057b4608bc14f77b1ed09bf7"
    static let INSTAGRAM_REDIRECT_URI = "https://afarrag.me/anything"
    static let INSTAGRAM_ACCESS_TOKEN = ""
    static let INSTAGRAM_SCOPE = "basic"
}

class InstagramIntergrationViewController: UIViewController {
    private var viewModel: InstagramIntegrationViewModel
    private let webView = WKWebView()

    init(viewModel: InstagramIntegrationViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        loadWebView()
    }
    
    func loadWebView() {
        let authURL = String(format: "%@?client_id=%@&redirect_uri=%@&response_type=token&scope=%@&DEBUG=True", arguments: [API.INSTAGRAM_AUTHURL,API.INSTAGRAM_CLIENT_ID,API.INSTAGRAM_REDIRECT_URI, API.INSTAGRAM_SCOPE])
        let urlRequest = URLRequest.init(url: URL.init(string: authURL)!)
        webView.load(urlRequest)
        webView.navigationDelegate = self.viewModel
    }
    
    override func loadView() {
        view = webView
    }
}
