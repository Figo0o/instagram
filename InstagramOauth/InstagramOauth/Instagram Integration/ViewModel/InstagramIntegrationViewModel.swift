//
//  InstagramIntegrationViewModel.swift
//  Instagram
//
//  Created by Ahmed Farrag on 05.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation
import UIKit
import WebKit

class InstagramIntegrationViewModel: NSObject {
    var didRecieveAccessToken: ((String) -> Void)?
}

extension InstagramIntegrationViewModel: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let requestURLString = (webView.url?.absoluteString)! as String
        if requestURLString.hasPrefix(API.INSTAGRAM_REDIRECT_URI) {
            guard let range: Range<String.Index> = requestURLString.range(of: "#access_token=") else { return }
            
            handleAuth(acessToken: String(requestURLString[range.upperBound...]))
        }
    }

    func handleAuth(acessToken: String) {
        didRecieveAccessToken?(acessToken)
    }
}
