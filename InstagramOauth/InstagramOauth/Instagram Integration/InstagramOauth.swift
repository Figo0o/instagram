//
//  InstagramOauth.swift
//  InstagramOauth
//
//  Created by Ahmed Farrag on 06.10.19.
//  Copyright © 2019 Ahmed Farrag. All rights reserved.
//

import Foundation
import UIKit


public class InstagramOauth {
    public static var shared: InstagramOauth {
        return InstagramOauth()
    }
    
    private init() {}
        
    public func login(from presentingViewController: UIViewController, with completion: @escaping ((String) -> Void)) {
        let viewModel = InstagramIntegrationViewModel()
        viewModel.didRecieveAccessToken = completion
        let viewController = InstagramIntergrationViewController(viewModel: viewModel)
        
        presentingViewController.present(viewController,
                                         animated: true,
                                         completion: nil)
    }
}
